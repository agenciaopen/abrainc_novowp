<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
</head>

<body>

<?php
$hostname = "localhost";
$database = "salaoimovel";
$username = "root";
$password = "";
$mysqlq = mysqli_connect($hostname, $username, $password, $database); 
mysqli_select_db($mysqlq, $database);

?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style>
.imageCard{
height: 200px;
background-size:cover!important;
border-bottom:1px solid #e0e0e0;
}
.cardx{
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;	
}
.card-title{
height:60px;
color:#0086ac;
margin-bottom:0;
}
.valor{
	color:#ff5916;
	font-weight:bold;
}
.valor span{
	font-weight:100;
	font-size:14px;
}
.card-text{
	font-size:14px;
}

@media (min-width: 768px){
.col-md-3 {
    max-width: 23%;
}
}
</style>
<div class="container">
<div class="row">

<?php
	$query = mysqli_query ($mysqlq,
	"SELECT 
		sl_imovel.*, sl_bairro.bairro,sl_bairro.idCidade, sl_cidade.cidade,sl_fase.fase,sl_tipo.tipo,sl_incorporadora.incorporadora
	FROM 
		sl_imovel,sl_cidade, sl_bairro, sl_fase, sl_incorporadora, sl_tipo
	WHERE
		sl_imovel.idBairro = sl_bairro.id
		AND sl_bairro.idCidade = sl_cidade.id
		AND sl_imovel.idFase = sl_fase.id		
		AND sl_imovel.idTipo = sl_tipo.id				
		AND sl_imovel.idIncorporadora = sl_incorporadora.id
	GROUP BY id
	ORDER by RAND() LIMIT 0,8") or die (mysqli_error($mysqlq));
		while ($data = mysqli_fetch_array($query))
		{
			$id = $data['id'];
			$nome = utf8_encode($data['nome']);
			$cidade= utf8_encode($data['cidade']);
			$bairro= utf8_encode($data['bairro']);
			$tipo = utf8_encode($data['tipo']);
			$fase = utf8_encode($data['fase']);
			$incorporadora = utf8_encode($data['incorporadora']);						
			$valor = $data['valor'];
			$valor = number_format( $valor , 0 , ',' , '.' );
			$publish = $data['publish'];
			
			$queryF = mysqli_query ($mysqlq,"SELECT sl_foto.* FROM sl_foto WHERE idImovel = $id AND destaque = 1") or die (mysqli_error($mysqlq));
			
			$dataF = mysqli_fetch_array($queryF);
			
			$idFoto = $dataF['id'];
			$destaque = utf8_encode($dataF['destaque']);


  	if($dataF != ''){ 
	$bg = "https://salaodoimovelsp.com.br/images/" . $id . "-" . $idFoto . ".jpg";
	}else{
	$bg = "https://salaodoimovelsp.com.br/images/no-foto.jpg";
	}

?>

<div class="col-md-3 cardx m-2 p-0">
	<div class="imageCard" style="background:url(<?=$bg;?>) no-repeat center #e0e0e0">
    </div>
  <div class="card-body">
    <h5 class="card-title"><?=$nome;?></h5>
    <p class="card-text"><?=$cidade?> - <?=$bairro?></p>
    <h5 class="valor text-right"><span>a partir de </span>R$ <?=$valor;?></h5>
    <a href="#" class="btn btn-primary w-100">+ Detalhes</a>
  </div>
</div>


<? } ?>			

</div>
</div>
</body>
</html>