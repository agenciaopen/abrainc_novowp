<? /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lista de Imoveis clicados</title>
<meta name="googlebot" content="noindex" />
</head>

<body>

<style>
.mainform{
	border:1px solid #e0e0e0;
	border-radius:5px;
	background:f0f0f0;
	padding:10px 25px;
}
</style>


<?php
$hostname = "localhost";
$database = "salaoimovel";
$username = "root";
$password = "";
$mysqlq = mysqli_connect($hostname, $username, $password, $database); 
mysqli_select_db($mysqlq, $database);

?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<link rel="stylesheet" href="css/style.css" />


<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<style>
table.dataTable.no-footer, table.dataTable thead th, table.dataTable thead td {
    border-bottom: 1px solid #e0e0e0;
}


</style>

<script>
		
$(document).ready(function() {
    $('#grid-basic').DataTable( {
        "paging":   false    } );
} );	  
</script>

<div class="container-fluid" style="background:#000; padding:15px;">
<center>
<a href="https://salaodoimovelsp.com.br/"><img src="https://salaodoimovelsp.com.br/wp-content/uploads/2019/09/logo-site-da.png" width="200" /></a>
</center>
</div>
<div class="container">

<h2>Lista de Clicks de Imóveis no site</h2>

<div class="table-responsive mainclass-form">
<table id="grid-basic" class="table table-condensed table-hover table-striped">
  <thead>
    <tr>
    <th scope="col">Nome</th>
    <th scope="col">Cidade</th>
    <th scope="col">Bairro</th>
    <th scope="col">Incorporadora</th>
    <th scope="col">Valor</th>    
    <th scope="col">Clicks</th>        
          
  </tr>
  </thead>
  <tbody>
  <tr>
<?php
	$query = mysqli_query ($mysqlq,
	"SELECT 
		sl_imovel.*, sl_bairro.bairro,sl_bairro.idCidade, sl_cidade.cidade,sl_fase.fase,sl_tipo.tipo,sl_incorporadora.incorporadora
	FROM 
		sl_imovel,sl_cidade, sl_bairro, sl_fase, sl_incorporadora, sl_tipo
	WHERE
		sl_imovel.idBairro = sl_bairro.id
		AND sl_bairro.idCidade = sl_cidade.id
		AND sl_imovel.idFase = sl_fase.id		
		AND sl_imovel.idTipo = sl_tipo.id				
		AND sl_imovel.idIncorporadora = sl_incorporadora.id
		AND sl_imovel.publish = 1
	GROUP BY id
	ORDER by click DESC") or die (mysqli_error($mysqlq));
		while ($data = mysqli_fetch_array($query))
		{
			$id = $data['id'];
			$nome = utf8_encode($data['nome']);
			$cidade= utf8_encode($data['cidade']);
			$bairro= utf8_encode($data['bairro']);
			$tipo = utf8_encode($data['tipo']);
			$fase = utf8_encode($data['fase']);
			$click = utf8_encode($data['click']);
			$incorporadora = utf8_encode($data['incorporadora']);
$valor = $data['valor'];
			$valor = number_format( $valor , 0 , ',' , '.' );
			$publish = $data['publish'];
			
			$queryF = mysqli_query ($mysqlq,"SELECT sl_foto.* FROM sl_foto WHERE idImovel = $id AND destaque = 1") or die (mysqli_error($mysqlq));
			
			$dataF = mysqli_fetch_array($queryF);
			
			$idFoto = $dataF['id'];
			$destaque = utf8_encode($dataF['destaque']);
		
?>
<? /*    
    <td align="center">
    <? if($dataF != ''){ ?>
		<img src="https://salaodoimovelsp.com.br/images/<? echo $id . '-' . $idFoto . '.jpg'; ?>" width="50" height="50" />
	<? }else{ ?>
	    <img src="https://salaodoimovelsp.com.br/images/no-foto.jpg" width="50" height="50" />
    <? } ?>
    </td>
*/?>	
    <td><strong><?=$nome;?></strong></td>
    <td><?=$cidade;?></td>
    <td><?=$bairro;?></td>
    <td><?=$incorporadora;?></td>
    <td><?=$valor;?></td>
    <td><h5><?=$click;?></h5></td>

  </tr>
<? } ?>
</tbody>
</table>
</div>

</div>

</body>
</html>