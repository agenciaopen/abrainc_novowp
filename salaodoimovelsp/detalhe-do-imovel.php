<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
</head>

<body>
<?php

$idImovel = 1;


$hostname = "localhost";
$database = "salaoimovel";
$username = "root";
$password = "";
$mysqlq = mysqli_connect($hostname, $username, $password, $database); 
mysqli_select_db($mysqlq, $database);

?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style>
.imageCard{
height: 200px;
background-size:cover!important;
border-bottom:1px solid #e0e0e0;
}
.cardx{
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;	
}
.card-title{
height:60px;
color:#0086ac;
margin-bottom:0;
}
.valor{
	color:#ff5916;
	font-weight:bold;
}
.valor span{
	font-weight:300;
	font-size:14px;
}
.card-text{
	font-size:14px;
}
.foto-destaque{
	border:1px solid #e0e0e0;
}

@media (min-width: 768px){
.col-md-3 {
    max-width: 23%;
}
}
</style>
<?
$query = mysqli_query ($mysqlq,
	"SELECT 
		sl_imovel.*, sl_bairro.bairro,sl_bairro.idCidade, sl_cidade.cidade,sl_fase.fase,sl_tipo.tipo,sl_incorporadora.incorporadora
	FROM 
		sl_imovel,sl_cidade, sl_bairro, sl_fase, sl_incorporadora, sl_tipo
	WHERE
		sl_imovel.idBairro = sl_bairro.id
		AND sl_bairro.idCidade = sl_cidade.id
		AND sl_imovel.idFase = sl_fase.id		
		AND sl_imovel.idTipo = sl_tipo.id				
		AND sl_imovel.idIncorporadora = sl_incorporadora.id
		AND publish = 1 
		AND	sl_imovel.id = $idImovel") or die (mysqli_error($mysqlq));
		
		$data = mysqli_fetch_array($query);

			$nome = utf8_encode($data['nome']);
			$cidade= utf8_encode($data['cidade']);
			$bairro= utf8_encode($data['bairro']);
			$tipo = utf8_encode($data['tipo']);
			$fase = utf8_encode($data['fase']);
			$incorporadora = utf8_encode($data['incorporadora']);						
			$valor = $data['valor'];
			$valor = number_format( $valor , 0 , ',' , '.' );
	$mapa = $data['mapa'];
	$quartos = $data['quartos'];
	$descricao = utf8_encode($data['descricao']);
	$endereco = utf8_encode($data['endereco']);	
	
?>

<div class="row">
<div class="col-md-4">
<?
$queryF = mysqli_query ($mysqlq,"SELECT sl_foto.* FROM sl_foto WHERE idImovel = $idImovel AND destaque = 1") or die (mysqli_error($mysqlq));
			
			$dataF = mysqli_fetch_array($queryF);
			
			$idFoto = $dataF['id'];
			$destaque = utf8_encode($dataF['destaque']);

	if($dataF != ''){ ?>
		<img src="https://salaodoimovelsp.com.br/images/<? echo $idImovel . '-' . $idFoto . '.jpg'; ?>" class="img-fluid foto-destaque" />
	<? }else{ ?>
	    <img src="https://salaodoimovelsp.com.br/images/no-foto.jpg" class="img-fluid foto-destaque" />
    <? } ?>			
    
</div>

<div class="col-md-6">
<h2><?=$nome;?></h2>
<h4><?=$incorporadora;?></h4>
<h5><?=$cidade . " - " . $bairro;?></h5>
<p><?=$endereco;?></p>
<h5 class="valor"><span>a partir de </span>R$ <?=$valor;?></h5>
<p><?=$descricao;?></p>
</div>
<div class="col-md-12">
<h4>Galeria de Fotos</h4>
<div class="row">
<?
$query = mysqli_query ($mysqlq,
	"SELECT 
		sl_foto.*
	FROM 
		sl_foto
	WHERE
		destaque != 1
		AND tipo = 1
		AND idImovel = $idImovel") or die (mysqli_error($mysqlq));
		while ($data = mysqli_fetch_array($query))
		{
			$idFoto = $data['id'];
			$tipo = utf8_encode($data['tipo']);

?>

<div class="col-md-3">
	<img src="https://salaodoimovelsp.com.br/images/<? echo $idImovel . '-' . $idFoto . '.jpg'; ?>" class="img-fluid foto-destaque" />
</div>


<? } ?>			
</div>
</div>

<div class="col-md-12">
<h4>Plantas</h4>
<div class="row">
<?
$query = mysqli_query ($mysqlq,
	"SELECT 
		sl_foto.*
	FROM 
		sl_foto
	WHERE
		destaque != 1
		AND tipo = 2
		AND idImovel = $idImovel") or die (mysqli_error($mysqlq));
		while ($data = mysqli_fetch_array($query))
		{
			$idFoto = $data['id'];
			$tipo = utf8_encode($data['tipo']);

?>

<div class="col-md-3">
	<img src="https://salaodoimovelsp.com.br/images/<? echo $idImovel . '-' . $idFoto . '.jpg'; ?>" class="img-fluid foto-destaque" />
</div>


<? } ?>
<? if($mapa != ''){ ?>
<div class="col-md-12">
<h4>Localização</h4>

<iframe src="<?=$mapa;?>" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


</div>
<?  } ?>

</div>
</div>

</div>
	
    
    
</body>
</html>