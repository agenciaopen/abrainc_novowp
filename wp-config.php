<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'site-homologacao-novo' );


/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );


/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );


/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$&VRV.3.|^2fQi}S:M@^-S+U-v4SxiT+0:`k.P1t(9|+2oo/Y<38-5<MWqk+ZRH]');
define('SECURE_AUTH_KEY',  '/bm,R)xd~2rnV5}/:sn:g2Fgw#V&BW%X:2LVt+.?xq|{Nu}]XYdLy&7D)Dc1UTeP');
define('LOGGED_IN_KEY',    'YxHI+*WQh{p.3:%#;V~@Ws!`PZC;FO|P{1%S*bk>+hHt%T{2u}RmVA;yAa:/eu$r');
define('NONCE_KEY',        ';R@%OMOk==x-APhd|J+N(9+z]3,:r+Tr`U2,-:s:QOGzL>z%==7C6CGIVP]*@<w|');
define('AUTH_SALT',        'iC`z+&r/>P}M%+AzqY@8<>|cHmt&z~QsewKK=aXOPdMpd3<X@8$!C4]jM1Rt8u%(');
define('SECURE_AUTH_SALT', '1~!I0ezELFpY,.-~-BYb8_$f(z;=mHVL50F|sq$0GH;eP~YQ<x,8X+0K;UsB5=vv');
define('LOGGED_IN_SALT',   '{w,;|I(WiH>%>+ r9|AFPag3|s+K9D@HWI39+D.[Ej03$GQ}Ms9].2TG8uLUZ3^B');
define('NONCE_SALT',       'XL,JreDN]wF^>+R7Yfa55Hj;pBc*,)1LT`PePqqSRhd4}~ir>=CP-;F$~OCdSlqK');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
